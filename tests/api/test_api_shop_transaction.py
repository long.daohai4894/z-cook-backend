import json

from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse
from starlette.testclient import TestClient

from app.schemas.sche_base import ResponseSchemaBase
from tests.api import APITestCase


class TestGetBeneficiary(APITestCase):
    ISSUE_KEY = 'O2OSTAFF-550'

    def test_000_success(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_page_page_size_null(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . get default các trường page =1 , page_size = 10
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_transactionFilter_is_null(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . get default trường transactionFilter = [0,1,2,3,4,5,10]
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_shopId_is_null(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . get default tất cả các trường shopId
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_shop_ids(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_0(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_1(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_2(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_3(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_4(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_5(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_transactionFilter_is_10(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass
    def test_000_success_have_shop_id_not_exists(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_page_size_1000_record(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - get list shop transaction
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_001_reportDate_required(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Không truyền params reportDate
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_page_invalid(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền params page là string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_page_size_invalid(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền params page là string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_page_size_invalid(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền params page là string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_transactionFilter_invalid(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền params page không thuộc
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_http_code_414_shop_id_too_long(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền params page không thuộc
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_shop_id_invalid(self, client: TestClient):
        """
        Test a get list shop transaction
        Step by step:
        - Get list shop transaction
        - Truyền thêm params shop_id string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_401_unauthorized(self, client: TestClient):
        """
            Test api get list shop transaction, response code 401
            Step by step:
            - Gọi API get list shop transaction
            - Đầu ra mong muốn:IAM
                . status code: 401
                . code: 991
        """
        pass

    def test_500_can_connect_db(self, client: TestClient):
        """
            Test api get list shop transaction, response code 500
            Step by step:
            -
            - Đầu ra mong muốn:IAM
                . status code: 500
                . code: 991
        """
        pass

    def test_999_internal_error(self, client: TestClient):
        """
            Test api get list beneficiary, response code 999
            Step by step:
            - Gọi API get list beneficiary lỗi
            - Đầu ra mong muốn:
                . status code: 500
                . code: 999
        """
        res = JSONResponse(
            status_code=500,
            content=jsonable_encoder(
                ResponseSchemaBase().custom_response(
                    '999', "Hệ thống đang bảo trì, quý khách vui lòng thử lại sau"
                )
            )
        )
        assert res.status_code == 500 and json.loads(res.body) == {
            "code": "999",
            "message": "Hệ thống đang bảo trì, quý khách vui lòng thử lại sau"
        }
