import json

from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse
from starlette.testclient import TestClient

from app.schemas.sche_base import ResponseSchemaBase
from tests.api import APITestCase


class TestJobProcessByDay(APITestCase):
    ISSUE_KEY = 'O2OSTAFF-566'

    def test_000_success(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day, only add params page, pageSize, sortBy, order, reportDate
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_with_1_staff_id(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day, only add params page, pageSize, sortBy, order, reportDate,
        array with 1 staff_id
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_with_many_staff_id(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day, only add params page, pageSize, sortBy, order, reportDate,
        array with n staff_id
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_page_page_size_null(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day
        - Dau ra mong muon:
            . get default các trường page =1 , page_size = 10
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_staffId_is_null(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day
        - Dau ra mong muon:
            . get default tất cả các staffId
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_staffIds(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_000_success_have_page_size_1000_record(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - get list Job Process By Day
        - Dau ra mong muon:
            . Status_code: 200
            . code: 000
        """
        pass

    def test_001_reportDate_required(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - Get list Job Process By Day
        - Không truyền params reportDate
        - Dau ra mong muon:
            . Status_code: 400
            . code: 007
        """
        pass

    def test_001_page_is_string_invalid(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - Get list Job Process By Day
        - Truyền params page là string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_page_size_is_string_invalid(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - Get list Job Process By Day
        - Truyền params page là string
        - Dau ra mong muon:
            . Status_code: 422
            . code: 001
        """
        pass

    def test_001_page_lower_than_0_invalid(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - Get list Job Process By Day
        - Truyền params page < 0
        - Dau ra mong muon:
            . Status_code: 400
            . code: 003
        """
        pass

    def test_001_page_size_lower_than_0_invalid(self, client: TestClient):
        """
        Test a get list Job Process By Day
        Step by step:
        - Get list Job Process By Day
        - Truyền params page < 0
        - Dau ra mong muon:
            . Status_code: 400
            . code: 002
        """
        pass

    def test_500_can_connect_db(self, client: TestClient):
        """
            Test api get list Job Process By Day, response code 500
            Step by step:
            -
            - Đầu ra mong muốn:IAM
                . status code: 500
                . code: 991
        """
        pass

    def test_999_internal_error(self, client: TestClient):
        """
            Test api get list beneficiary, response code 999
            Step by step:
            - Gọi API get list beneficiary lỗi
            - Đầu ra mong muốn:
                . status code: 500
                . code: 999
        """
        res = JSONResponse(
            status_code=500,
            content=jsonable_encoder(
                ResponseSchemaBase().custom_response(
                    '999', "Hệ thống đang bảo trì, quý khách vui lòng thử lại sau"
                )
            )
        )
        assert res.status_code == 500 and json.loads(res.body) == {
            "code": "999",
            "message": "Hệ thống đang bảo trì, quý khách vui lòng thử lại sau"
        }
