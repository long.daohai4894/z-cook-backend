from fastapi import APIRouter

from app.api.api_v1 import api_healthcheck, api_ingredient, api_meal

router = APIRouter()

router.include_router(api_healthcheck.router, tags=["healthcheck"], prefix="/healthcheck")
router.include_router(api_ingredient.router, tags=["ingredient"], prefix="/ingredient")
router.include_router(api_meal.router, tags=["meal"], prefix="/meal")
