import logging
from typing import Any, List

from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from app.db.base import get_db

logger = logging.getLogger()
router = APIRouter()


@router.get("")
def get(
        staffId: List[int] = Query(None),
        db: Session = Depends(get_db)
) -> Any:
    """
    API get tiến độ công việc của Sale theo ngày và lũy kế theo tháng

    Danh sách Params:
    - staffId[]: Danh sách sale id
    - reportDate: Ngày cần lấy báo cáo tiến độ. Format %Y-%m-%d
    """
    return
