import logging
from typing import Any, List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db.base import get_db
from app.schemas.sche_base import DataResponse
from app.schemas.sche_ingredient import Ingredient, IngredientParamsRequest, IngredientResponse
from app.schemas.sche_meal import Meal, MealListRequest, MealListResponse, MealRandomRequest
from app.services import service_meal

logger = logging.getLogger()
router = APIRouter()


@router.get("")
async def get_list(
    query=Depends(MealListRequest),
    db: Session = Depends(get_db)
):
    result = await service_meal.get(db, query)
    return DataResponse().success_response(data=result)


@router.get("/detail/{mealId}")
async def get_detail(mealId: str, db: Session = Depends(get_db)):
    result = await service_meal.get_detail(db, mealId)
    return DataResponse().success_response(data=result)


@router.post("/random")
async def get_random_list(
    query=Depends(MealRandomRequest),
    db: Session = Depends(get_db)
):
    return await service_meal.get_random_list(db, query)
