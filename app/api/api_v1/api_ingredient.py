import logging
from typing import Any, List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db.base import get_db
from app.schemas.sche_base import DataResponse
from app.schemas.sche_ingredient import Ingredient, IngredientParamsRequest, IngredientResponse
from app.services import service_ingredient

logger = logging.getLogger()
router = APIRouter()


@router.get("", response_model=DataResponse[List[IngredientResponse]])
async def get_list(
        query=Depends(IngredientParamsRequest),
        db: Session = Depends(get_db)
):
    return await service_ingredient.get(db, query)


@router.post("")
async def create(
        ingredient: Ingredient,
        db: Session = Depends(get_db)
) -> Any:
    """
    API create ingredient
    """
    await service_ingredient.create(db, ingredient.dict())
    return
