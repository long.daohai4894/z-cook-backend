from fastapi import APIRouter
from fastapi import Response

from app.helpers.check_database_connect import check_database_connect
from app.helpers.minio_handler import MinioHandler
from app.schemas.sche_base import ResponseSchemaBase

router = APIRouter()


@router.get("/live", response_model=ResponseSchemaBase)
async def get():
    return {
        "code": "000",
        "message": "Health check success"
    }


@router.get("/ready", response_model=ResponseSchemaBase)
async def get():
    is_database_connect, output = check_database_connect()
    return {
        "code": "000",
        "message": "Ready check success"
    } if is_database_connect else Response({"message": "Ready check false"}, status_code=400)
