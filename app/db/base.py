from typing import Generator

import motor.motor_asyncio

from app.core.config import settings


async def get_db() -> Generator:
    client = motor.motor_asyncio.AsyncIOMotorClient(settings.MONGO_DETAILS)
    try:
        db = client[settings.DB_NAME]
        yield db
    finally:
        client.close()
