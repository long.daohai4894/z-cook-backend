from typing import Optional

from pydantic import BaseModel, Field

from app.helpers.paging import PaginationParams
from app.schemas.sche_base import MappingByFieldName


class IngredientParamsRequest(PaginationParams):
    name: Optional[str]
    id: Optional[str]


class Ingredient(BaseModel):
    id: str = Field(...)
    name: str = Field(...)
    image: Optional[str]


class MealIngredient(MappingByFieldName):
    src: str = Field(...)
    description: Optional[str]
    quantity: Optional[float]# = Field(None, alias="quantitative")
    unit: Optional[str]
    name: Optional[str]


class IngredientResponse(Ingredient):
    pass


class IngredientDetail(Ingredient):
    id: str
