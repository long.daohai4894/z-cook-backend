from typing import List, Optional

from pydantic import BaseModel, Field

from app.helpers.paging import PaginationParams
from app.schemas.sche_base import PaginationSchema
from app.schemas.sche_ingredient import MealIngredient


class MealStep(BaseModel):
    description: str
    imgs: Optional[List[str]]


class MealBase(BaseModel):
    id: str = Field(...)
    image: str = Field(...)
    name: str = Field(...)
    category: int = Field(...)
    ingredients: Optional[List[MealIngredient]]


class Meal(MealBase):
    src: Optional[str]
    content: Optional[str]
    steps: Optional[List[MealStep]]
    time: Optional[str]
    people: Optional[str]


class MealHistory(BaseModel):
    history: List[List[MealBase]]


class MealRandomInfo(BaseModel):
    name: str
    image: str
    type: int


class MealRandomResponse(BaseModel):
    meals: List[MealRandomInfo]


class MealListRequest(PaginationParams):
    name: Optional[str] = ""
    category: Optional[int]


class MealListResponse(BaseModel):
    meals: List[MealBase]
    pagination: PaginationSchema


class MealRandomRequest(BaseModel):
    history: Optional[List[List[str]]]
    current_menu: Optional[List[str]]
    loved: Optional[List[str]]
    allergic: Optional[List[str]]
