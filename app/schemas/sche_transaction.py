from typing import List, Optional

from pydantic import BaseModel, Field
from pydantic.schema import date

from app.helpers.paging import PaginationParams
from app.schemas.sche_base import DataResponse, PaginationSchema, MappingByFieldName


class TransactionItemResponse(MappingByFieldName):
    id: int
    shop_id: int = Field(None, alias='shopId')
    number_of_tran: int = Field(None, alias='numberOfTran')
    number_of_tran_k1: int = Field(None, alias='numberOfTranK1')
    number_of_tran_k2: int = Field(None, alias='numberOfTranK2')
    number_of_tran_k3: int = Field(None, alias='numberOfTranK3')
    number_of_tran_k4: int = Field(None, alias='numberOfTranK4')
    number_of_tran_acm: int = Field(None, alias='numberOfTranAcm')
    number_of_tran_last_month: int = Field(None, alias='numberOfTranLastM')
    tran_amount_mtd_last_month: int = Field(0, alias="tranMountLastM")
    gmv: int


class TransactionDataResponse(BaseModel):
    shopTransactions: List[TransactionItemResponse]
    reportDate: date
    pagination: PaginationSchema


class TransactionResponse(DataResponse):
    data: TransactionDataResponse


class TransactionRequest(PaginationParams):
    reportDate: date


class TransactionQuery(TransactionRequest):
    shopId: Optional[List[int]]
    transactionFilter: Optional[List[int]]
