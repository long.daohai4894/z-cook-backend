from copy import deepcopy
from typing import List
from collections import Counter

from bson.objectid import ObjectId

from app.core.config import settings
from app.helpers.paging import PaginationParams, paginate
from app.helpers.validate import helper_data
from app.schemas.sche_meal import MealListRequest, MealListResponse, MealRandomRequest

NUM_MAJOR_DISH = 2
NUM_SOUP_DISH = 1
NUM_DESSERT_DISH = 1

OCCURRENCES = 3
TOP_K_INGREDIENT = 2

async def create():
    pass


async def get(db, query: MealListRequest):
    params = PaginationParams(
        page=query.page,
        pageSize=query.pageSize,
        sortBy=query.sortBy,
        order=query.order
    )
    db_query = {}
    if query.name:
        db_query["name"] = {"$regex": query.name}
    if query.category is not None:
        db_query["category"] = query.category
    result = await paginate(db, collection=settings.FOOD_COLLECTION_NAME, query=db_query, params=params)
    return {
        "meals" : result.data,
        "pagination": result.metadata
    }

async def get_detail(db, id: str):
    count_items = await db[settings.FOOD_COLLECTION_NAME].count_documents({ "_id": ObjectId(id) })
    if count_items == 0:
        return None
    else:
        cursor = db[settings.FOOD_COLLECTION_NAME].find({ "_id": ObjectId(id) }).limit(1)
        meal_data = (await cursor.to_list(None))[0]
        meal_data["id"] = str(meal_data["_id"])
        del meal_data["_id"]
        return meal_data


async def get_name_in_list_id(db, items: list):
    item_id = [ObjectId(item) for item in items]
    items_object = await db.dish_staging.find({"_id": {"$in": item_id}}).to_list(None)
    res = []
    for item in items_object:
        name = item["ingredients"][0]["name"]
        if name:
            res.append(name)
        if len(item["ingredients"]) > 1 and item["ingredients"][1]["name"]:
            res.append(item["ingredients"][1]["name"])
    return '|'.join(res)

def aggregate_dish(collection, query, sample_size):
    return collection.aggregate([
        query,
        {'$sample': {'size': sample_size}}
    ])

async def aggregate_menu(collection, query: dict):
    result = []
    major_query = query.copy()
    major_query['$match']['category'] = {'$eq': 0}
    major = aggregate_dish(collection, major_query, NUM_MAJOR_DISH)
    result.extend(await major.to_list(None))

    soup_query = query.copy()
    soup_query['$match']['category'] = {'$eq': 1}
    soup = aggregate_dish(collection, soup_query, NUM_SOUP_DISH)
    result.extend(await soup.to_list(None))

    dessert_query = query.copy()
    dessert_query['$match']['category'] = {'$eq': 2}
    dessert = aggregate_dish(collection, dessert_query, NUM_DESSERT_DISH)
    result.extend(await dessert.to_list(None))
    return result

async def get_ingredient_from_history(db, history: List[List[str]]):
    ingredients = []
    for menu in history:
        ingredient = await get_name_in_list_id(db, items=menu)
        ingredients.append(ingredient)
    ingredients_counts = Counter(ingredients)
    ingredients_final = []
    print("ingredients_counts", ingredients_counts)
    for k, v in ingredients_counts.items():
        if len(k) > 0 and v > OCCURRENCES:
            ingredients_final.append(k)
    return '|'.join(ingredients_final[:1])

async def get_random_list(db, query: MealRandomRequest):
    """
    các món ăn mới không nằm trong danh sách menu hiện tại
    các nguyên liệu không có trong danh sách allergic
    lấy ra nguyên liệu từ loved => id món ăn => tìm món ăn có nguyên liệu đó
    từ history => mảng 2 chiều id và ngày ==> món ăn nguyên liệu yêu thích => món ăn yêu thích
    """
    loved = await get_name_in_list_id(db, items=query.loved)
    current_meal = await get_name_in_list_id(db, items=query.current_menu)
    allergic = "|".join(allergic for allergic in query.allergic)
    ingredients_favorite = await get_ingredient_from_history(db, query.history)
    if current_meal:
        allergic = allergic + '|' + current_meal
    
    queries = [{'$match': {}}]

    if allergic:
        query_allergic = deepcopy(queries)[-1]
        query_allergic['$match']['name'] = {'$not': {'$regex': allergic, '$options': '/^/i'}}
        queries.append(query_allergic)
    
    if loved:
        query_love = deepcopy(queries)[-1]
        query_love['$match']['name'] = {'$regex': loved, '$options': '/^/i'}
        queries.append(query_love)

    if ingredients_favorite:
        print("ingredients_favorite", ingredients_favorite)
        query_favorite = deepcopy(queries)[-1]
        if query_favorite['$match'].get('name') and query_favorite['$match']['name'].get('$regex', None):
            query_favorite['$match']['name']['$regex'] += '|' + ingredients_favorite
        else:
            query_favorite['$match']['name'] = {'$regex': ingredients_favorite, '$options': '/^/i'}
        queries.append(query_favorite)

    # lấy món ăn từ suggest history, nếu thiếu lấy query trước
    queries.reverse()
    result_dic = {
        0: [],
        1: [],
        2: [],
    }
    final_result = []
    for idx_query, query in enumerate(queries):
        result = await aggregate_menu(db.dish_staging, query)
        print("[Query]: ", query, "=>>>>>" ,[(str(dish['_id']), dish['name']) for dish in result])
        if len(queries) > 1 and idx_query == 0 and len(result) > 0:
            dish = result[0]
            result_dic[dish["category"]].append(dish)
        else:
            for dish in result:
                result_dic[dish["category"]].append(dish)
        
        if len(result_dic[0]) >= NUM_MAJOR_DISH and \
            len(result_dic[1]) >= NUM_SOUP_DISH and \
            len(result_dic[2]) >= NUM_DESSERT_DISH:
            break
    
    for idx, num in enumerate([NUM_MAJOR_DISH, NUM_SOUP_DISH, NUM_DESSERT_DISH]):
        for idx2 in range(num):
            final_result.append(result_dic[idx][idx2])
    await helper_data(final_result)
    return final_result
