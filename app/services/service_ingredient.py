from app.helpers.paging import paginate
from app.schemas.sche_ingredient import IngredientParamsRequest


async def get(db, query: IngredientParamsRequest):
    inside_query = {}
    if query.name is not None:
        inside_query["name"] = {"$regex": query.name}
    ingredients = await paginate(db, collection="ingredient", query=inside_query, params=query)

    return ingredients


async def create(db, ingredient: dict):
    db.ingredient.insert_one(ingredient)
