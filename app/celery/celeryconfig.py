from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
    'runs-every-days-at-0am_utc': {
        'task': 'app.celery.main.cron_dwh',
        'schedule': crontab(minute=5, hour='0-8', day_of_week="0-6")
    },
}
