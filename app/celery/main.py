# import logging
#
import io
import logging
from datetime import date

import pandas as pd
from celery import Celery
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import sessionmaker

from app.core.config import settings
from app.helpers.minio_handler import MinioHandler
from app.helpers.time_helper import get_yesterday
from app.models import Transaction

cel = Celery(broker=settings.BROKER_URL)
cel.config_from_object('app.celery.celeryconfig')

logger = logging.getLogger()

engine = create_engine(settings.DATABASE_URL, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@cel.task
def cron_dwh():
    logger.info("Start")
    db = SessionLocal()
    if check_record_today(db):
        logger.info("Have records exists")
        return

    minio = MinioHandler.get_instance()
    file_parquet = minio.download_file_parquet_in_director("report/qr_shop.v3/")
    data_frame_dwh = pd.read_parquet(io.BytesIO(file_parquet), engine="pyarrow")

    if data_frame_dwh.iloc[0].get("report_date") != get_yesterday():
        logger.info("Data not new")
        return

    if len(data_frame_dwh.index) > 0:
        total, objs = 0, []
        for key, item in data_frame_dwh.iterrows():
            objs.append(Transaction(
                shop_id=item.get("shop_id"),
                number_of_tran=item.get("number_of_tran"),
                number_of_tran_k1=item.get("number_of_tran_w_1_7"),
                number_of_tran_k2=item.get("number_of_tran_w_8_14"),
                number_of_tran_k3=item.get("number_of_tran_w_15_21"),
                number_of_tran_k4=item.get("number_of_tran_w_22_end"),
                number_of_tran_acm=item.get("number_of_tran_acm"),
                number_of_tran_last_month=item.get("number_of_tran_last_m"),
                gmv=item.get("gmv"),
                report_date=item.get("report_date"),
                tran_amount_mtd=item.get("tran_amount_mtd"),
                tran_amount_mtd_last_month=item.get("tran_amount_mtd_last_month")
            ))
            total += 1
            if total % 10000 == 0 or total == len(data_frame_dwh.index):
                db.bulk_save_objects(objs)
                db.commit()
                objs = []
                logger.info(f'Step: Insert into transaction. Total: {total}')

    logger.info("Success")
    return None


def check_record_today(db):
    # get first data
    records_today_in_mysql = db.query(func.count(Transaction.id)).filter(
        Transaction.report_date == get_yesterday()).scalar()
    return records_today_in_mysql


@cel.task
def test(arg):
    print(arg)
