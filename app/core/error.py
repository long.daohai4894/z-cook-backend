class ErrorCode(object):
    ERROR_001_REQUIRED_FIELD_NOT_NULL = '001'
    ERROR_002_PAGE_SIZE_LARGE_THAN_0 = '002'
    ERROR_003_PAGE_LARGE_THAN_0 = '003'
    ERROR_004_FIELD_VALUE_INVALID = '004'
    ERROR_005_ORDER_VALUE_INVALID = '005'
    ERROR_006_SEARCH_PARAMS_INVALID = '006'
    ERROR_007_REPORT_DATE_IS_REQUIRED = '007'
    ERROR_040_UNAUTHORIZED = '040'
    ERROR_992_ERROR = '992'
    ERROR_999_SERVER = '999'


class ErrorMessage(object):
    MESSAGE_001_REQUIRED_FIELD_NOT_NULL = 'Các trường thông tin bắt buộc không được bỏ trống'
    MESSAGE_002_PAGE_SIZE_LARGE_THAN_0 = 'Số lượng phần tử trong trang tìm kiếm phải lớn hơn 0 và nhỏ hơn 1000'
    MESSAGE_003_PAGE_LARGE_THAN_0 = 'Số thứ tự của trang hiện tại phải lớn hơn hoặc bằng 0'
    MESSAGE_004_FIELD_VALUE_INVALID = 'Trường dữ liệu không hợp lệ'
    MESSAGE_005_ORDER_VALUE_INVALID = 'Chiều sắp xếp phải là "desc" hoặc "asc"'
    MESSAGE_006_SEARCH_PARAMS_INVALID = 'Các trường tìm kiếm không hợp lệ'
    MESSAGE_007_REPORT_DATE_IS_REQUIRED = 'reportDate là bắt buộc'
    MESSAGE_040_UNAUTHORIZED = 'unauthorized'
    MESSAGE_992_ERROR = 'có lỗi xảy ra, vui lòng liên hệ admin'
    MESSAGE_999_SERVER = "Hệ thống đang bảo trì, quý khách vui lòng thử lại sau"


error_code = ErrorCode()
message = ErrorMessage()
