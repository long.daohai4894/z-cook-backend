import logging

logger = logging.getLogger()


class StorageAbstract(object):

    def presigned_get_object(self, bucket_name, object_name):
        pass

    def make_bucket(self) -> str:
        pass

    def presigned_get_object(self, bucket_name, object_name):
        pass

    def check_file_name_exists(self, bucket_name, file_name):
        pass

    def put_object(self, file_data, file_name, content_type):
        pass

    def normalize_file_name(self, file_name):
        pass



