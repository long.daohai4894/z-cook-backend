import enum


class TransactionType(enum.Enum):
    S0 = 0
    S1 = 1
    S2 = 2
    S3 = 3
    S4 = 4
    S5 = 5
    S10 = 10

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_
