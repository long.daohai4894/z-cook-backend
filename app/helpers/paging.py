import logging
from abc import ABC, abstractmethod
from contextvars import ContextVar
from typing import Optional, Generic, Sequence, Type, TypeVar

from pydantic import BaseModel, root_validator
from pydantic.generics import GenericModel

from app.core import error_code, message
from app.helpers.exception_handler import ValidateException
from app.schemas.sche_base import ResponseSchemaBase, PaginationSchema

T = TypeVar("T")
C = TypeVar("C")

logger = logging.getLogger()


class PaginationParams(BaseModel):
    pageSize: Optional[int] = 200
    page: Optional[int] = 1
    sortBy: Optional[str] = 'id'
    order: Optional[str] = 'desc'

    @root_validator()
    def validate_data(cls, data):
        page = data.get("page")
        page_size = data.get("pageSize")
        order = data.get("order")

        if page <= 0:
            raise ValidateException(
                error_code.ERROR_003_PAGE_LARGE_THAN_0, message.MESSAGE_003_PAGE_LARGE_THAN_0)
        if page_size <= 0:
            raise ValidateException(
                error_code.ERROR_002_PAGE_SIZE_LARGE_THAN_0, message.MESSAGE_002_PAGE_SIZE_LARGE_THAN_0)

        if order not in ['asc', 'desc']:
            raise ValidateException(
                error_code.ERROR_005_ORDER_VALUE_INVALID, message.MESSAGE_005_ORDER_VALUE_INVALID)

        return data


class BasePage(ResponseSchemaBase, GenericModel, Generic[T], ABC):
    data: Sequence[T]

    class Config:
        arbitrary_types_allowed = True

    @classmethod
    @abstractmethod
    def create(cls: Type[C], code: str, message: str, data: Sequence[T], metadata: PaginationSchema) -> C:
        pass  # pragma: no cover


class Page(BasePage[T], Generic[T]):
    metadata: PaginationSchema

    @classmethod
    def create(cls, code: str, message: str, data: Sequence[T], metadata: PaginationSchema) -> "Page[T]":
        return cls(
            code=code,
            message=message,
            data=data,
            metadata=metadata
        )


PageType: ContextVar[Type[BasePage]] = ContextVar("PageType", default=Page)


async def paginate(db, collection, query, params: Optional[PaginationParams]) -> Page:
    code = '000'
    message = 'Thành công'

    total = await db[collection].count_documents(query)
    skips = params.pageSize * (params.page - 1)

    cursor = db[collection].find(query).skip(skips).limit(params.pageSize)
    data = await cursor.to_list(None)
    for item in data:
        item["id"] = str(item["_id"])
        del item["_id"]
    metadata = PaginationSchema(
        currentPage=params.page,
        pageSize=params.pageSize,
        totalItems=total
    )
    return PageType.get().create(code, message, data, metadata)
